##### portainer setup
*unter shared_ordner/portainer die hier liegende docker-compose.yml hinzufügen* \
sudo pcs resource create portainer_docker ocf:heartbeat:docker-compose dirpath=/data/portainer binpath=/data/docker-compose op monitor interval=120s \
*auch ip ressource erstellen und beide ressourcen in gruppe hinzufügen* 

*danach ueber weboberfläche einrichten (http://resourceip:9000) -> admin user ertstellen* \
**erstes environment einrichten, dieses läuft nur auf einer node:** 
- *unter environments add environment* 
- *docker standalone, edge agent, name für erste node* 
- *server url kann bleiben* 
- *create* 
- *linux-kommando evtl. mal testen ob funktioniert -> abkopieren und ausführen, schauen ob alles ok ist* 
- *danach das kommando in die unter dem hier liegenden ordner portainer-agent/docker-compose.yml parsen (ids usw austauschen)* 
- *die portainer-agent/docker-compose.yml datei lokal irgendwo ablegen (es wird dann noch ein weiteres environment für die zweite node erstellt, welche wieder andere daten hat*
- *ressource erstellen (NICHT in eine gruppe hinzufügen), z.B. mit* 
```
sudo pcs resource create docker_portainer_agent_one ocf:heartbeat:docker-compose dirpath=/data/portainer-agent/ binpath=/data/docker-compose op monitor interval=120s
```
- wobei unter /data/portainer-agent/ die docker-compose.yml mit den geparsten daten liegen muss 
- *da dieser dienst nur auf node1 laufen muss, wird er auf diese node restrained:*
- - sudo pcs constraint location docker_portainer_agent_one avoids two \
- - wobei docker_portainer_agent_one die ressource ist und two die node, auf welche der dienst nicht laufen soll \

**das selbe nochmal für 2te node mit geänderten daten für 2te node**


