before running vagrantfile: *export VAGRANT_EXPERIMENTAL="disks"* \
*/etc/hosts -> 127.0.0.1 muss in die richtige ip der maschine umgewandelt werden, sonst funktioniert es nicht* \
\
**docker-compose plugin muss extra installiert werden** \
dazu nach /usr/lib/ocf/resource.d/heartbeat gehen und die hier liegende datei *docker-compose* hineinkopieren \
mit *sudo chmod 755 /usr/lib/ocf/resource.d/heartbeat/docker-compose* rechte setzen 

**Damit das ip_registry feld auf enabled bleibt:** \
*sudo pcs property set stonith-enabled=false*

**docker-compose binary** \
*sudo curl -SL https://github.com/docker/compose/releases/download/v2.13.0/docker-compose-linux-x86_64 -o /data/docker-compose* \
mit *sudo chmod 755 /data/docker-compose* rechte setzen

**docker registry ressource erstellen kommando:** \
sudo pcs resource create **\<name>** ocf:heartbeat:docker-compose **dirpath=**\<pfad wo docker-compose.yml liegt> **binpath=**/usr/local/bin/docker-compose **ymlfile=**\<name der yml datei falls sie nicht docker-compose.yml heißt>

**create cluster:** \
- *fix /etc/hosts as mentioned before*
- sudo pcs auth one two -u hacluster -p 123
- sudo pcs cluster setup --name mycluster one two
- sudo pcs cluster start --all
- sudo pcs property set stonith-enabled=false
- sudo pcs property set no-quorum-policy=ignore
- *put docker-compose binary under /data and put correct rights*
- *put docker-compose shellscript here like mentioned before
- *put docker-compose.yml from here under /data*
- sudo pcs resource create docker_registry ocf:heartbeat:docker-compose dirpath=/data binpath=/data/docker-compose op monitor interval=120s
- sudo pcs resource create ip_registry ocf:heartbeat:IPaddr2 ip=192.168.56.77 cidr_netmask=28 op monitor interval=120s
- sudo pcs resource group add registry docker-registry
- sudo pcs resource group add registry ip-registry
- *get Dockerfile from apache day 1 for example*
- *allow registry for http use, next 2 steps on both machines:*
- sudo vi /etc/docker/daemon.json
- contents: 
```
{
    "insecure-registries" : [ "192.168.56.77:5000", "192.168.56.70:5000" ]
}
```
- sudo systemctl restart docker
- *put Dockerfile here to current working directory*
- sudo docker build -t web .
- sudo docker tag web 192.168.56.77:5000/web
- sudo docker push 192.168.56.77:5000/web
- *run webserver from image web (image from Dockerfile here)*
- sudo pcs resource create ip_apache ocf:heartbeat:IPaddr2 ip=192.168.56.78 cidr_netmask=28 op monitor interval=120s
- sudo pcs resource create docker_apache ocf:heartbeat:docker image=192.168.56.77:5000/web allow_pull=true run_opts="-p 80:80" op monitor interval=120s
- sudo pcs resource group add apache docker_apache
- sudo pcs resource group add apache ip_apache
