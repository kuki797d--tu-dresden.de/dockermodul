**run all examples inside the container of Vagrantfile**

**docker-machine installation:** \
base=https://github.com/docker/machine/releases/download/v0.14.0 &&
  curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine &&
  sudo install /tmp/docker-machine /usr/local/bin/docker-machine


###### Vagrant increase disksize:
vagrant plugin install vagrant-disksize \
config.disksize.size = '20GB' \
vagrant halt \
vagrant up \
vagrant ssh \
sudo cfdisk /dev/sda \
*resize the partition & write & quit* \
sudo xfs_growfs /dev/sda1

